### Workshop Strapi HEADLESS CMS Back + Frameworks Js Front (React / Next , Vue / Nuxt / Gridsomes)

- Créer 3 dossiers sur vos machines
    - blog
    - ecommerce
    - corporate

- A l'intèrieur de chacun, mettez en place chacun des templates suivant
- pour le blog vous pouvez choisir entre 5 technos disponible


---

#### 1 : Blog 

##### Front
https://github.com/strapi/strapi-starter-next-blog

https://github.com/strapi/strapi-starter-gridsome-blog

https://github.com/strapi/strapi-starter-react-blog

https://github.com/strapi/strapi-starter-vue-blog


##### Back
https://github.com/strapi/strapi-template-blog

---

#### 2 : Ecommerce

##### Front
https://github.com/strapi/strapi-starter-nuxt-e-commerce

https://github.com/strapi/strapi-starter-next-ecommerce

##### Back
https://github.com/strapi/strapi-template-ecommerce

---

#### 3 :Corporate

##### Front
https://github.com/strapi/strapi-starter-next-corporate

##### Back
https://github.com/strapi/strapi-template-corporate

---
